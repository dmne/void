#!/usr/bin/env bash

sudo xbps-install -S flatpak xz vpm
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

