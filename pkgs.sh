#!/usr/bin/env bash

sudo vpm ar void-repo-nonfree
sudo vpm ar void-repo-multilib
sudo vpm ar void-repo-multilib-nonfree
sudo vpm ar void-repo-debug
sudo vpm sync
sudo vpm up
sudo vpm i xtools tty-clock seatd htop btop nnn fish nwg-look wbg fuzzel rofi rofi-calc wl-clipboard cliphist elogind pcmanfm polkit-gnome zoxide  xorg-server-xwayland  xdg-user-dirs xdg-desktop-portal-wlr xdg-desktop-portal-gtk wf-recorder udiskie timg telegram-desktop swappy  starship speedtest-cli slurp ripgrep qt6-wayland-devel qt5-wayland-devel  qbittorrent dbus  pkg-config pixman-devel curl wget git pfetch  trash-cli util-linux  rpi-imager python3-pipx mpc mpd mpv yt-dlp  cmake mesa-vulkan-radeon mesa-vdpau mesa-vaapi mesa-dri mesa-ati-dri lolcat-c  lm_sensors NoiseTorch bashmount base-devel libayatana-indicator  libayatana-appindicator gvfs gvfs-mtp gvfs-gphoto2  grim fuse-devel  file-roller extra-cmake-modules meson elogind-devel p7zip p7zip-unrar unrar moc NetworkManager  network-manager-applet python3-psutil inxi pipewire alsa-pipewire wireplumber
sudo vpm i river Waybar wlopm lswt dunst fzf micro kitty amdvlk nvtop
xdg-user-dirs-update